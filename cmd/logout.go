package cmd

import (
	"github.com/spf13/cobra"
	"tesonet-task/helpers"
	"tesonet-task/logout"
)

//Main functionality of the logout command
func LogoutCommand() *cobra.Command {
	cmdLogout := &cobra.Command{
		Use:   "logout",
		Short: "Functionality for logout command.",
		Run:   logoutHandler,
	}

	cmdLogout.Flags().Bool("database", false, "Server storing file")
	return cmdLogout
}

//Handler function for login command
func logoutHandler(cmd *cobra.Command, args []string) {
	database, err := cmd.Flags().GetBool("database")
	helpers.PanicErr(err)
	if database {
		logout.LogoutDB()
	} else {
		logout.LogoutFile()
	}
}
