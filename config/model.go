package config

import (
	sql "github.com/jmoiron/sqlx"
)

var (
	Conf       *Config
	db         *sql.DB
	dbUri      string
	driverName string
)

type (
	DbConfig struct {
		Server     string `yaml:"server"`
		DriverName string `yaml:"driverName"`
		DbName     string `yaml:"dbname,omitempty"`
		User       string `yaml:"user,omitempty"`
		Password   string `yaml:"password,omitempty"`
		Charset    string `yaml:"charset,omitempty"`
	}

	Config struct {
		Db      DbConfig `yaml:"db"`
		LogFile string   `yaml:"logFile"`
	}
)
