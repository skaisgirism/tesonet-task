package cmd

import (
	"github.com/spf13/cobra"
	"tesonet-task/helpers"
	"tesonet-task/servers"
)

//Main functionality of the servers command
func ServersCommand() *cobra.Command {
	cmdServers := &cobra.Command{
		Use:   "servers",
		Short: "Functionality for servers command.",
		RunE:  serversHandler,
	}
	cmdServers.Flags().Bool("local", false, "Server fetch location indicator")
	cmdServers.Flags().Bool("database", false, "Server storing database")
	return cmdServers
}

//Handles the functionality of the servers command and selects what data stores to use.
func serversHandler(cmd *cobra.Command, args []string) error {
	local, err := cmd.Flags().GetBool("local")
	helpers.PanicErr(err)

	database, err := cmd.Flags().GetBool("database")
	helpers.PanicErr(err)

	if local {
		if database {
			servers.GetServersFromDb()
		} else {
			servers.GetServersFromFile()
		}
	} else {
		serverList := servers.GetServers(database)
		if database {
			servers.AddServersToDb(serverList)
		} else {
			servers.AddServersToFile(serverList)
		}
	}
	return nil
}
