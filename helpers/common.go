package helpers

import (
	log "github.com/sirupsen/logrus"
)

//Helper function for non critical error checking
func CheckErr(err error) {
	if err != nil {
		log.Println("Error: ", err)
	}
}

//Helper function for critical error checking
func PanicErr(err error) {
	if err != nil {
		panic(err)
	}
}
