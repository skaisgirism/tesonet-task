package main

import (
	"tesonet-task/cmd"
	"tesonet-task/config"
	"os"
	"fmt"
	"io"
	log "github.com/sirupsen/logrus"
)

func init() {
	config.GetConfig("config.yml")
}

func main() {
	f, err := os.OpenFile("logs.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", err)
	}
	mw := io.MultiWriter(os.Stdout, f)
	log.SetFormatter(&log.JSONFormatter{})
	defer f.Close()
	log.SetOutput(mw)


	cmd.AddCommands()
}
