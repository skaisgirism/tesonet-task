package servers

type tokenStruct struct {
	Token string `json:"token"`
}

type serverStruct struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Distance int    `json:"distance"`
}
