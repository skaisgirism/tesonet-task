package logout

import (
	"fmt"
	"os"
	"tesonet-task/config"
	"tesonet-task/helpers"
)

//Logout functionality for the database
func LogoutDB() {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	_, err = db.Exec("TRUNCATE TABLE login")
	helpers.PanicErr(err)
	fmt.Println("Logged out successfully.")
}

//Logout functionality for local file
func LogoutFile() {
	loginFile, err := os.OpenFile("data/logins.json", os.O_RDWR, 0666)
	defer loginFile.Close()
	helpers.CheckErr(err)
	err = loginFile.Truncate(0)
	helpers.CheckErr(err)
	fmt.Println("Logged out successfully.")
}
