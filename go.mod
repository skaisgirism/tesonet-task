module tesonet-task

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/gommon v0.3.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	golang.org/x/crypto v0.0.0-20200115085410-6d4e4cb37c7d
	gopkg.in/yaml.v2 v2.2.7
)
