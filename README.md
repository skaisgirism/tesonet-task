# Tesonet-task

This is a Tesonet assigned console app that handles log ins and log outs
checks if the log in credentials are correct, shows and saves servers received 
from API to either local file or mySQL database.

## Persistent data stores
Tesonet-task uses two persistent data stores **mySQL** and **storing to local files**. 
By default all commands store their data into local files located in the **/data** directory.


To use **mySQL** as the persistent data store you would have to add ``--database`` flag to every command.
However storing to **mySQL** will only work if you have the tables in the database. 
The database is set up using the **config.yaml** file. The functionality of the setup can be found in the **/config** directory.

To create the database use this query:
```
CREATE DATABASE `tesonet` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */
```
To create the tables use these two queries:
```
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `token` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```
```
CREATE TABLE `server` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `distance` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

## Project setup
```
go build
```
To start the project you have to build the app. To do that call go build command while standing in the tesonet-task repository.

## Project commands
To manage the commands I used the [Cobra library](https://github.com/spf13/cobra).

#### Main commands:

```
./tesonet-task login --username "YOUR USERNAME" --password "YOUR PASSWORD" 
```
Command **login** requires two parameters **username** and **password** for it to work. 
This command sends out a request to the api using users credentials to receive a token. 
Without the correct username and password a user will not be able to log in receive a token.

By default this command saves everything into a **./data/logins.json** file.
```
./tesonet-task logout
```
Command **logout** clears the user information from the selected data store(by default it uses the local data file).
```
./tesonet-task servers
```
Command **servers** gets the token for the logged in user from the selected data store and using the token sends 
out a request to receive servers. Afterwards it prints out the servers to the console and stores all the data to the selected data store.
```
./tesonet-task servers --local
```
Command **servers --local** has an extra option **--local** added to the simple **servers** command.
This command gets the before stored servers from the selected database and prints them out to the console.
This command does not do additional storing.

## To Do
- Clearing the databases after logging out(depends on future functionalities)
- Logging to file
- Test coverage