package servers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"tesonet-task/config"
	"tesonet-task/helpers"
	"tesonet-task/login"
	"text/tabwriter"
)

const URLservers = "http://playground.tesonet.lt/v1/servers"

//Get server list from the API using the authorization token we received from GetToken()
func GetServers(database bool) []serverStruct {
	var token string
	if database {
		token = login.GetTokenFromDb()
	} else {
		token = login.GetTokenFromFile()
	}
	req, err := http.NewRequest("GET", URLservers, nil)
	helpers.CheckErr(err)

	req.Header.Add("authorization", "Bearer "+token)

	res, err := http.DefaultClient.Do(req)
	helpers.CheckErr(err)

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	helpers.CheckErr(err)

	serverList := make([]serverStruct, 0)

	err = json.Unmarshal(body, &serverList)
	helpers.CheckErr(err)

	fmt.Println("There are ", len(serverList), " servers available")
	printServers(serverList)

	return serverList
}

//Printing servers in a set structure
func printServers(serverList []serverStruct) {
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', tabwriter.Debug|tabwriter.Debug)
	for _, server := range serverList {
		fmt.Fprintf(w, "%s \t %d \n", server.Name, server.Distance)
	}
	w.Flush()
}

//Clearing data from the server table in the database
func clearDb() {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	_, err = db.Exec("TRUNCATE TABLE server")
	helpers.PanicErr(err)
}

func AddServersToDb(servers []serverStruct) {
	clearDb()
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	for i := 0; i < len(servers); i++ {
		_, err = db.Exec("INSERT INTO server("+
			"name, "+
			"distance)"+
			" VALUES(?, ?)",
			servers[i].Name,
			servers[i].Distance)
		helpers.CheckErr(err)
	}
}

func GetServersFromDb() {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	var serverList []serverStruct

	rows, err := db.Query("SELECT id, name, distance FROM `server`")

	for rows.Next() {
		var server serverStruct
		err := rows.Scan(
			&server.Id,
			&server.Name,
			&server.Distance)
		helpers.PanicErr(err)

		serverList = append(serverList, server)
	}

	fmt.Println("There are ", len(serverList), " servers available")
	printServers(serverList)
}

func AddServersToFile(servers []serverStruct) {
	_, err := os.Create("data/servers.json")
	helpers.PanicErr(err)

	data, _ := json.Marshal(servers)

	ioutil.WriteFile("data/servers.json", data, 0644)
}

func GetServersFromFile() {
	data, err := ioutil.ReadFile("data/servers.json")
	helpers.CheckErr(err)
	var serverList []serverStruct
	err = json.Unmarshal(data, &serverList)
	helpers.CheckErr(err)

	fmt.Println("There are ", len(serverList), " servers available")
	printServers(serverList)
}
