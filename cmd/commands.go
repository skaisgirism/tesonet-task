package cmd

import (
	"github.com/spf13/cobra"
)

//Adding commands
func AddCommands() {
	cmd := &cobra.Command{
		Use:          "tesonet",
		Short:        "Tesonet task",
		SilenceUsage: true,
	}
	cmd.AddCommand(LoginCommand())
	cmd.AddCommand(ServersCommand())
	cmd.AddCommand(LogoutCommand())
	cmd.Execute()

}
