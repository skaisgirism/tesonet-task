package login

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	log "github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strings"
	"tesonet-task/config"
	"tesonet-task/helpers"
	"golang.org/x/crypto/bcrypt"
)

const URLtoken = "http://playground.tesonet.lt/v1/tokens"

//Getting token from the API using login credentials
func GetToken(login LoginStruct) LoginStruct {
	payload := strings.NewReader(`{"username": "` + login.Username + `", "password":  "` + login.Password + `"}`)

	req, err := http.NewRequest("POST", URLtoken, payload)
	helpers.CheckErr(err)

	req.Header.Add("content-type", "application/json")

	res, err := http.DefaultClient.Do(req)
	helpers.CheckErr(err)

	if res.StatusCode == 401 {
		log.Fatal("Error couldn't get token. Response: " + res.Status)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	helpers.CheckErr(err)

	login.Password = encryptPassword(login.Password)

	err = json.Unmarshal(body, &login)
	helpers.CheckErr(err)

	return login
}

//Inserting login to the database
func AddLoginToDb(login LoginStruct) {
	clearDb()
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	_, err = db.Exec("INSERT INTO login("+
		"username, "+
		"password, "+
		"token)"+
		" VALUES(?, ?, ?)",
		login.Username,
		login.Password,
		login.Token)
	helpers.PanicErr(err)
	fmt.Println("User " + login.Username + " logged in successfully.")
}

//Inserting login to a local file
func AddLoginToFile(login LoginStruct) {
	_, err := os.Create("data/logins.json")
	helpers.PanicErr(err)

	data, err := json.Marshal(login)
	helpers.PanicErr(err)

	ioutil.WriteFile("data/logins.json", data, 0644)
	fmt.Println("User " + login.Username + " logged in successfully.")

}

//Getting token from the database
func GetTokenFromDb() string {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)
	var login []LoginStruct

	db.Select(&login, "SELECT token FROM `login` WHERE id=1")

	if login == nil {
		log.Fatal("You should log in first.")
	}
	return login[0].Token
}

//Getting token from the local file
func GetTokenFromFile() string {
	data, err := ioutil.ReadFile("data/logins.json")
	helpers.PanicErr(err)
	var login LoginStruct
	json.Unmarshal(data, &login)

	if login.Token == "" {
		log.Fatal("You should log in first.")
	}
	return login.Token
}

//Clearing the local database
func clearDb() {
	db, err := config.Conf.GetDb()
	helpers.PanicErr(err)

	_, err = db.Exec("TRUNCATE TABLE login")
	helpers.PanicErr(err)
}

//Encrypt password using bcrypt library
func encryptPassword (password string) string {
	cost := bcrypt.DefaultCost
	hash, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	helpers.PanicErr(err)
	password = string(hash)
	return password
}
