package cmd

import (
	"github.com/spf13/cobra"
	"tesonet-task/helpers"
	"tesonet-task/login"
)

//Main functionality of the log in command
func LoginCommand() *cobra.Command {
	cmdLogin := &cobra.Command{
		Use:   "login",
		Short: "Functionality for login command.",
		Run:   loginHandler,
	}

	cmdLogin.Flags().String("username", "", "Login Username")
	cmdLogin.Flags().String("password", "", "Login Password")
	cmdLogin.Flags().Bool("database", false, "Login storing database")
	cmdLogin.MarkFlagRequired("username")
	cmdLogin.MarkFlagRequired("password")

	return cmdLogin
}

//Handler function for login command
func loginHandler(cmd *cobra.Command, args []string) {
	var data login.LoginStruct
	var err error
	data.Username, err = cmd.Flags().GetString("username")
	helpers.CheckErr(err)
	data.Password, err = cmd.Flags().GetString("password")
	helpers.CheckErr(err)

	database, err := cmd.Flags().GetBool("database")

	if database {
		login.AddLoginToDb(login.GetToken(data))
	} else {
		login.AddLoginToFile(login.GetToken(data))
	}
}
